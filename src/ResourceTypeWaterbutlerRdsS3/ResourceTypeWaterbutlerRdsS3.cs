using Amazon.S3;
using Amazon.S3.Model;
using Coscine.Configuration;
using Coscine.ECSManager;
using Coscine.ResourceTypeBase;
using Coscine.WaterbutlerHelper;
using Coscine.WaterbutlerHelper.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeWaterbutlerRdsS3
{
    public class ResourceTypeWaterbutlerRdsS3 : ResourceTypeDefinition
    {
        private readonly string _rdsS3KeyPrefix;
        private readonly string _userKeyPrefix;

        private readonly WaterbutlerInterface _waterbutlerInterface;

        private readonly EcsManager _rdsS3EcsManager;
        private readonly EcsManager _userEcsManager;

        private readonly string _accessKey;
        private readonly string _secretKey;

        private readonly string _provider = "rds";

        private readonly List<string> _readRights;
        private readonly List<string> _writeRights;

        public ResourceTypeWaterbutlerRdsS3(string name, IConfiguration gConfig, ResourceTypeConfigurationObject resourceTypeConfiguration) : base(name, gConfig, resourceTypeConfiguration)
        {
            _waterbutlerInterface = new WaterbutlerInterface(Configuration, new DataSourceService(new HttpClient()));
            _rdsS3KeyPrefix = resourceTypeConfiguration.Config["rdss3Key"];
            _userKeyPrefix = resourceTypeConfiguration.Config["userKey"];
            _rdsS3EcsManager = new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = Configuration.GetString($"{_rdsS3KeyPrefix}/manager_api_endpoint"),
                    NamespaceName = Configuration.GetString($"{_rdsS3KeyPrefix}/namespace_name"),
                    NamespaceAdminName = Configuration.GetString($"{_rdsS3KeyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = Configuration.GetString($"{_rdsS3KeyPrefix}/namespace_admin_password"),
                    ObjectUserName = Configuration.GetString($"{_rdsS3KeyPrefix}/object_user_name"),
                    ReplicationGroupId = Configuration.GetString($"{_rdsS3KeyPrefix}/replication_group_id"),
                }
            };

            _accessKey = Configuration.GetString($"{_rdsS3KeyPrefix}/object_user_name");
            _secretKey = Configuration.GetString($"{_rdsS3KeyPrefix}/object_user_secretkey");

            _userEcsManager = new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = Configuration.GetString($"{_userKeyPrefix}/manager_api_endpoint"),
                    NamespaceName = Configuration.GetString($"{_userKeyPrefix}/namespace_name"),
                    NamespaceAdminName = Configuration.GetString($"{_userKeyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = Configuration.GetString($"{_userKeyPrefix}/namespace_admin_password"),
                    ObjectUserName = Configuration.GetString($"{_userKeyPrefix}/object_user_name"),
                    ReplicationGroupId = Configuration.GetString($"{_userKeyPrefix}/replication_group_id"),
                }
            };

            _readRights = new List<string> { "read", "read_acl" };
            _writeRights = new List<string> { "read", "read_acl", "write", "execute", "privileged_write", "delete" };
        }

        public override async Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(prefix, _provider, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in Waterbutler under \"{prefix}\".");
            }

            var entries = new List<ResourceEntry>();

            foreach (var info in infos)
            {
                entries.Add(new ResourceEntry(info.Path, info.IsFile, info.Size == null ? 0 : (long)info.Size, null, null, info.Created == null ? new DateTime() : (DateTime)info.Created, info.Modified == null ? new DateTime() : (DateTime)info.Modified));
            }

            return entries;
        }

        public override async Task<ResourceEntry> GetEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, _provider, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos[0].IsFile)
            {
                throw new Exception("Not a file.");
            }

            return new ResourceEntry(key, infos[0].IsFile, (long)infos[0].Size, null, null, infos[0].Created == null ? new DateTime() : (DateTime)infos[0].Created, infos[0].Modified == null ? new DateTime() : (DateTime)infos[0].Modified);
        }

        public override async Task StoreEntry(string id, string key, Stream body, Dictionary<string, string> options = null)
        {
            var url = await GetEntryStoreUrl(key.TrimStart('/'), null, options);

            HandleResponse(UploadObject(url, body));
        }

        private static HttpWebResponse UploadObject(Uri url, Stream stream)
        {
            HttpWebRequest httpRequest = WebRequest.Create(url) as HttpWebRequest;
            httpRequest.Method = "PUT";
            using (Stream dataStream = httpRequest.GetRequestStream())
            {
                var buffer = new byte[8000];
                int bytesRead = 0;
                while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    dataStream.Write(buffer, 0, bytesRead);
                }
            }
            return httpRequest.GetResponse() as HttpWebResponse;
        }

        public override async Task DeleteEntry(string id, string key, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, _provider, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under {key}.");
            }

            // Not a file
            if (infos.Count > 1 || !infos[0].IsFile)
            {
                throw new Exception("Not a file.");
            }

            HandleResponse(await _waterbutlerInterface.DeleteObjectAsync(infos[0], authHeader));
        }

        public override async Task<Stream> LoadEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);

            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, _provider, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos[0].IsFile)
            {
                throw new Exception("Not a file.");
            }

            // Do not dispose the response!
            // The stream is later accessed by the MVC, to deliver the file.
            // When disposed, the stream becomes invalid and can't be read.
            var response = await _waterbutlerInterface.DownloadObjectAsync(infos[0], authHeader);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStreamAsync();
            }

            return null;
        }

        protected static void HandleResponse(HttpResponseMessage httpResponseMessage)
        {
            var statuscode = (int)httpResponseMessage.StatusCode;
            if (statuscode == 403)
            {
                throw new RTDForbiddenException();
            }
            else if (statuscode == 404)
            {
                throw new RTDNotFoundException();
            }
            else if (statuscode >= 400 && statuscode < 600)
            {
                throw new RTDBadRequestException(httpResponseMessage.Content + httpResponseMessage.ReasonPhrase + httpResponseMessage.StatusCode);
            }
        }

        protected static void HandleResponse(HttpWebResponse httpWebResponse)
        {
            var statuscode = (int)httpWebResponse.StatusCode;
            if (statuscode == 403)
            {
                throw new RTDForbiddenException();
            }
            else if (statuscode == 404)
            {
                throw new RTDNotFoundException();
            }
            else if (statuscode >= 400 && statuscode < 600)
            {
                throw new RTDBadRequestException();
            }
        }

        public override async Task SetResourceReadonly(string id, bool status, Dictionary<string, string> options = null)
        {
            var policy = GenerateAccessPolicy(options["accessKey"], options["accessKeyWrite"], options["accessKeyRead"], id, status);

            var putRequest = new PutBucketPolicyRequest
            {
                BucketName = id,
                Policy = policy
            };

            var amazonConfig = new AmazonS3Config
            {
                ServiceURL = options["endpoint"],
                ForcePathStyle = true
            };

            using var client = new AmazonS3Client(_accessKey, _secretKey, amazonConfig);

            // Exception will be thrown on development systems.
            // Works on live.
            try
            {
                await client.PutBucketPolicyAsync(putRequest);
            }
#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
            catch (Exception)
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.
            {
            }
        }

        public override async Task CreateResource(Dictionary<string, string> options = null)
        {
            await _rdsS3EcsManager.CreateBucket(options["bucketname"], int.Parse(options["size"]));
            await _userEcsManager.CreateObjectUser(options["accessKeyRead"], options["secretKeyRead"]);
            await _userEcsManager.CreateObjectUser(options["accessKeyWrite"], options["secretKeyWrite"]);

            await _rdsS3EcsManager.SetUserAcl(options["accessKeyRead"], options["bucketname"], _readRights);
            await _rdsS3EcsManager.SetUserAcl(options["accessKeyWrite"], options["bucketname"], _writeRights);

            // Set to the read only value, if present.
            if (options?.ContainsKey("readonly") == true && bool.TryParse(options["readonly"], out var result))
            {
                await SetResourceReadonly(options["bucketname"], result, options);
            }
            else
            {
                await SetResourceReadonly(options["bucketname"], false, options);
            }
        }

        public override async Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string> options = null)
        {
            var amazonConfig = new AmazonS3Config
            {
                ServiceURL = options["endpoint"],
                ForcePathStyle = true
            };

            using var client = new AmazonS3Client(_accessKey, _secretKey, amazonConfig);
            long totalFileSize = 0;
            long fileCount = 0;
            var listRequest = new ListObjectsRequest()
            {
                BucketName = options["bucketname"]
            };

            ListObjectsResponse listResponse;
            do
            {
                listResponse = await client.ListObjectsAsync(listRequest);
                fileCount += listResponse.S3Objects.Count;
                totalFileSize += listResponse.S3Objects.Sum(x => x.Size);
                listRequest.Marker = listResponse.NextMarker;
            } while (listResponse.IsTruncated);

            return totalFileSize;
        }

        public override async Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string> options = null)
        {
            return await _rdsS3EcsManager.GetBucketQuota(options["bucketname"]);
        }

        public override async Task SetResourceQuota(string id, int quota, Dictionary<string, string> options = null)
        {
            await _rdsS3EcsManager.SetBucketQuota(options["bucketname"], quota);
        }

        public override async Task<Uri> GetEntryDownloadUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return await Task.Run(() =>
            {
                var amazonConfig = new AmazonS3Config
                {
                    ServiceURL = options["endpoint"],
                    ForcePathStyle = true
                };

                using var client = new AmazonS3Client(options["accessKey"], options["secretKey"], amazonConfig);
                var presignedUrl = client.GetPreSignedURL(new GetPreSignedUrlRequest()
                {
                    BucketName = options["bucketname"],
                    Key = key,
                    Verb = HttpVerb.GET,
                    // The Simulator uses HTTP and the live system HTTPS
                    Protocol = options["endpoint"].Contains("ecs-sim01.itc.rwth-aachen.de") ? Protocol.HTTP : Protocol.HTTPS,
                    // For now, expiry of a day is set, but this might be up to debate
                    Expires = DateTime.UtcNow.AddHours(24)
                });
                return new Uri(presignedUrl);
            });
        }

        public override async Task<Uri> GetEntryStoreUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return await Task.Run(() =>
            {
                var amazonConfig = new AmazonS3Config
                {
                    ServiceURL = options["endpoint"],
                    ForcePathStyle = true
                };

                using var client = new AmazonS3Client(options["accessKey"], options["secretKey"], amazonConfig);
                var presignedUrl = client.GetPreSignedURL(new GetPreSignedUrlRequest()
                {
                    BucketName = options["bucketname"],
                    Key = key,
                    Verb = HttpVerb.PUT,
                    // The Simulator uses HTTP and the live system HTTPS
                    Protocol = options["endpoint"].Contains("ecs-sim01.itc.rwth-aachen.de") ? Protocol.HTTP : Protocol.HTTPS,
                    // For now, expiry of a day is set, but this might be up to debate
                    Expires = DateTime.UtcNow.AddHours(24)
                });
                return new Uri(presignedUrl);
            });
        }

        public override async Task<ResourceTypeInformation> GetResourceTypeInformation()
        {
            var resourceTypeInformation = await base.GetResourceTypeInformation();
            resourceTypeInformation.IsQuotaAvailable = true;
            resourceTypeInformation.IsQuotaAdjustable = false;
            return await Task.FromResult(resourceTypeInformation);
        }

        private static string GenerateAccessPolicy(string accessKey, string writeKey, string accessKeyRead, string bucketname, bool isReadonly)
        {
            if (isReadonly)
            {
                return $@"{{
                          ""Version"": ""2012-10-17"",
                          ""Id"": ""null"",
                          ""Statement"":[
                              {{
                                  ""Action"": [""s3:GetObject"", ""s3:GetObjectAcl"", ""s3:GetObjectVersion""],
                                  ""Effect"": ""Allow"",
                                  ""Resource"": [""{bucketname}/*""],
                                  ""Principal"": [""{writeKey}"", ""{accessKey}"",""{accessKeyRead}""]
                              }},
                              {{
                                  ""Action"": [""s3:PutObject"", ""s3:DeleteObject"", ""s3:DeleteObjectVersion""],
                                  ""Effect"": ""Deny"",
                                  ""Resource"": [""{bucketname}/*""],
                                  ""Principal"": [""{writeKey}"", ""{accessKey}"",""{accessKeyRead}""]
                              }},
                          ]
                      }}";
            }
            else
            {
                return $@"{{
                          ""Version"": ""2012-10-17"",
                          ""Id"": ""null"",
                          ""Statement"":[
                              {{
                                  ""Action"": [""s3:PutObject"", ""s3:GetObject"", ""s3:GetObjectAcl"", ""s3:GetObjectVersion"", ""s3:DeleteObject"", ""s3:DeleteObjectVersion""],
                                  ""Effect"": ""Allow"",
                                  ""Resource"": [""{bucketname}/*""],
                                  ""Principal"": [""{writeKey}"", ""{accessKey}""]
                              }},
                              {{
                                  ""Action"": [""s3:GetObject"", ""s3:GetObjectAcl"", ""s3:GetObjectVersion""],
                                  ""Effect"": ""Allow"",
                                  ""Resource"": [""{bucketname}/*""],
                                  ""Principal"": [""{accessKeyRead}""]
                              }}
                          ]
                      }}";
            }
        }
    }
}