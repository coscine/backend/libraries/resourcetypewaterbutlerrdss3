﻿using Coscine.Configuration;
using Coscine.ECSManager;
using Coscine.ResourceLoader;
using Coscine.ResourceTypeBase;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Coscine.ResourceTypeWaterbutlerRdsS3.Test
{
    [TestFixture]
    public class ResourceTypeWaterbutlerRdsS3Tests
    {
        private readonly string _testPrefix = "Coscine-ResourceTypeWaterbutlerS3Tests-Tests";
        private readonly string _userKeyPrefix = "coscine/global/rds/ecs-rwth/users";

        private readonly IConfiguration _configuration = new ConsulConfiguration();

        private static readonly Dictionary<string, string> _configs = new Dictionary<string, string>
        {
            // RDS-S3
            { "rdss3", "coscine/global/rds/ecs-rwth/rds-s3" },
            { "rdss3ude", "coscine/global/rds/ecs-ude/rds-s3" },
            { "rdss3nrw", "coscine/global/rds/ecs-nrw/rds-s3" },
            { "rdss3tudo", "coscine/global/rds/ecs-tudo/rds-s3" }
        };

        private static IEnumerable<object[]> GetTestConfigs()
        {
            foreach (var kv in _configs)
            {
                yield return new object[] { kv.Key, kv.Value };
            }
        }

        private string _localAssemblyName;

        private long _quota;

        private Guid _guid;
        private string _bucketName;

        private string _readWriteSecretKey;

        private string _readUser;
        private string _writeUser;

        [SetUp]
        public void SetUp()
        {
            _localAssemblyName = "Coscine.ResourceTypeWaterbutlerRdsS3";

            _quota = 1;
            _guid = Guid.NewGuid();
            _bucketName = $"{_testPrefix}.{_guid}";
            _readWriteSecretKey = "VERY_S3cr3t_Key!!!";
            _readUser = $"{_testPrefix}.read_{_guid}";
            _writeUser = $"{_testPrefix}.write_{_guid}";
        }

        private EcsManager CreateManager(string keyPrefix)
        {
            return new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = _configuration.GetString($"{keyPrefix}/manager_api_endpoint"),
                    NamespaceName = _configuration.GetString($"{keyPrefix}/namespace_name"),
                    NamespaceAdminName = _configuration.GetString($"{keyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = _configuration.GetString($"{keyPrefix}/namespace_admin_password"),
                    ObjectUserName = _configuration.GetString($"{keyPrefix}/object_user_name"),
                    ReplicationGroupId = _configuration.GetString($"{keyPrefix}/replication_group_id"),
                }
            };
        }

        private ResourceTypeDefinition GetLocalResourceType(string type)
        {
            // Load the current assembly from the ref and not the locally installed.
            var assemblyName = Array.Find(Assembly.GetExecutingAssembly().GetReferencedAssemblies(), x => x.Name == _localAssemblyName);
            var assembly = Assembly.Load(assemblyName);
            var resourceType = ResourceTypeFactory.CreateResourceTypeObject(type, new ConsulConfiguration(), assembly);
            return resourceType;
        }

        private Dictionary<string, string> GetResourceOptions(string prefix)
        {
            var dictionary = new Dictionary<string, string>
            {
                { "accessKey", _configuration.GetString($"{prefix}/object_user_name") },
                { "secretKey", _configuration.GetString($"{prefix}/object_user_secretkey") },
                { "accessKeyRead", _readUser },
                { "secretKeyRead", _readWriteSecretKey },
                { "accessKeyWrite", _writeUser },
                { "secretKeyWrite", _readWriteSecretKey },
                { "bucketname", _bucketName },
                { "endpoint", _configuration.GetString($"{prefix}/s3_endpoint") },
                { "size", $"{_quota}" }
            };
            return dictionary;
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var kv in _configs)
            {
                var ecsManager = CreateManager(kv.Value);
                try
                {
                    ecsManager.DeleteBucket(_bucketName).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            var userEcsManager = CreateManager(_userKeyPrefix);

            try
            {
                userEcsManager.DeleteObjectUser(_readUser).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                userEcsManager.DeleteObjectUser(_writeUser).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestConstructor(string type, string configPrefix)
        {
            var resourceTypeConfiguration = new ResourceTypeConfigurationObject
            {
                Config = new Dictionary<string, string>
                {
                    { "rdss3Key", configPrefix },
                    { "userKey", _userKeyPrefix }
                }
            };
            var resourceTpye = new ResourceTypeWaterbutlerRdsS3(type, _configuration, resourceTypeConfiguration);
            Assert.NotNull(resourceTpye);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestCreate(string type, string configPrefix)
        {
            var rdsS3EcsManager = CreateManager(configPrefix);
            var userEcsManager = CreateManager(_userKeyPrefix);

            var resourceType = GetLocalResourceType(type);
            var options = GetResourceOptions(configPrefix);
            resourceType.CreateResource(options).Wait();

            var randomFileName = Guid.NewGuid();

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            options.Add("ContentLength", memoryStream.Length.ToString());

            resourceType.StoreEntry(null, $"/{randomFileName}", memoryStream, options).Wait();

            var entry = resourceType.GetEntry(null, $"/{randomFileName}", null, options).Result;

            Assert.NotNull(entry);

            var entries = resourceType.ListEntries(null, "/", options).Result;

            Assert.NotNull(entries);
            Assert.IsNotEmpty(entries);
            Assert.IsTrue(entries.Count == 1);
            Assert.IsTrue(entries[0].Key == entry.Key);

            resourceType.DeleteEntry(null, entry.Key, options).Wait();

            Assert.True(userEcsManager.DeleteObjectUser(_readUser).Result);
            Assert.True(userEcsManager.DeleteObjectUser(_writeUser).Result);
            Assert.True(rdsS3EcsManager.DeleteBucket(_bucketName).Result);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestResourceTypeInformation(string type, string _)
        {
            var resourceType = GetLocalResourceType(type);
            var resourceTypeInformation = resourceType.GetResourceTypeInformation().Result;
            Assert.IsTrue(resourceTypeInformation.IsQuotaAvailable);
            Assert.IsFalse(resourceTypeInformation.IsQuotaAdjustable);
        }
    }
}